
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


data = pd.read_csv('./data/prepared/prepared_rfm.csv')


# In[3]:


data.head(1)


# In[4]:


from sklearn.cluster import KMeans  
from sklearn.metrics import silhouette_score


# In[5]:


K_values = range(2, 10, 1)
progress = 0
scores = []
for K in K_values:
    kmeans = KMeans(n_clusters=K)
    kmeans.fit(data)
    # clusters = kmeans.transform(data)
    score = silhouette_score(data, kmeans.labels_)
    scores.append(score)
    
    progress += 1
    print ('\r' + str(int(100 * progress / len(K_values))), end='%')

    
    


# In[6]:


plt.plot(K_values, scores)


# In[7]:


scores, list(K_values)


# In[8]:


best_value = K_values[[i for i, j in enumerate(scores) if j == max(scores)][0]] # !
# best_value=4
kmeans = KMeans(n_clusters=K)
kmeans.fit(data)


# In[9]:


clusters_df = pd.DataFrame()


# In[10]:


clusters_df['SegmentNo'] = kmeans.labels_


# In[14]:


clusters_df['CustomerID'] = data['CustomerID']


# In[ ]:


clusters_df.to_csv('./results/segments_rfm_1.csv', index=False)

