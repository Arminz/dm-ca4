
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
# get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


data = pd.read_csv('./data/prepared/prepared_basic.csv')


# In[4]:


data.head(1)


# In[5]:


from sklearn.cluster import KMeans  
from sklearn.metrics import silhouette_score


# In[6]:


K_values = range(2, 10, 1)
progress = 0
scores = []
for K in K_values:
    kmeans = KMeans(n_clusters=K)
    kmeans.fit(data)
    score = silhouette_score(data, kmeans.labels_)
    scores.append(score)
    
    progress += 1
    print ('\r' + str(int(100 * progress / len(K_values))), end='%')

    
    


# In[7]:


# plt.plot(K_values, scores)


# In[8]:


# scores, list(K_values)


# In[14]:


best_value = K_values[[i for i, j in enumerate(scores) if j == max(scores)][0]] 
print('\nbest value for k : ', best_value)


# In[15]:


kmeans = KMeans(n_clusters=best_value)
kmeans.fit(data)


# In[16]:


clusters_df = pd.DataFrame()


# In[19]:


clusters_df['CustomerID'] = data['CustomerID']


# In[17]:


clusters_df['SegmentNo'] = kmeans.labels_


# In[21]:


clusters_df.to_csv('./results/segments_basic_kmeans.csv', index=False)


# In[30]:


clusters_df['SegmentNo'].value_counts()

