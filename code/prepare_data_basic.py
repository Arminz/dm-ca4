
# coding: utf-8

# In[40]:


import pandas as pd


# In[41]:


data = pd.read_csv('./data/onlineOrder_cleaned.csv')


# In[42]:


data.head(1)


# In[43]:


data = data[~data['CustomerID'].isna()]


# In[44]:


stockCodes = data['StockCode'].unique()


# In[45]:


customers = sorted(data['CustomerID'].unique())


# In[46]:


prepared_df = pd.DataFrame()


# In[47]:


prepared_df['CustomerID'] = customers


# In[53]:


progress = 0
for stockCode in stockCodes:
    prepared_df[stockCode] = data[data['StockCode'] == stockCode].groupby('CustomerID')['Quantity'].sum()[customers].values
    progress += 1
    print('\r'+str(int(100 * progress / len(stockCodes))), end='%')


# In[48]:


prepared_df.fillna(-100, inplace=True)


# In[49]:


# print(prepared_df.head(1))


# In[50]:


prepared_df['Country'] = data.groupby(['CustomerID'])['Country'].agg(pd.Series.mode)[customers].values


# In[52]:


# data.groupby(['CustomerID'])['Country'].agg(pd.Series.mode)


# In[31]:


# prepared_df[prepared_df['customer'] == 12361]['Country']


# In[ ]:


from sklearn.preprocessing import OneHotEncoder
ohe = OneHotEncoder()


# In[33]:


new_prepared_df = ohe.fit_transform(prepared_df['Country'].values.reshape(-1, 1)).toarray()


# In[34]:


ohe_df = pd.DataFrame(new_prepared_df, columns=ohe.categories_[0])
# ohe.categories_[0]


# In[35]:


# print(ohe_df.head(1))


# In[36]:


for column in ohe_df.columns:
    prepared_df[column] = ohe_df[column]


# In[37]:


prepared_df.drop('Country', axis=1, inplace=True)


# In[38]:


prepared_df.to_csv('./data/prepared/prepared_basic.csv', index=False)

