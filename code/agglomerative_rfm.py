
# coding: utf-8

# In[2]:


import pandas as pd
import matplotlib.pyplot as plt
# get_ipython().run_line_magic('matplotlib', 'inline')


# In[3]:


data = pd.read_csv('./data/prepared/prepared_rfm.csv')


# In[4]:


data.head(1)


# In[5]:


# print(data.head(1))


# In[7]:


data['mean'] = (data['RecencyScore'] + data['FrequencyScore'] + data['MonetaryScore']) / 3


# In[8]:


from sklearn.cluster.hierarchical import AgglomerativeClustering  
from sklearn.metrics import silhouette_score


# In[9]:


K_values = range(2, 10, 1)
progress = 0
scores = []
for K in K_values:
    model = AgglomerativeClustering(n_clusters=K)
    model.fit(data)
    # clusters = kmeans.transform(data)
    score = silhouette_score(data, model.labels_)
    scores.append(score)
    
    progress += 1
    print ('\r' + str(int(100 * progress / len(K_values))), end='%')

    
    


# In[10]:


# plt.plot(K_values, scores)


# In[11]:


scores, list(K_values)


# In[12]:


best_value = K_values[[i for i, j in enumerate(scores) if j == max(scores)][0]] # !
model = AgglomerativeClustering(n_clusters= best_value)
model.fit(data)


# In[13]:


clusters_df = pd.DataFrame()


# In[14]:


clusters_df['SegmentNo'] = model.labels_


# In[16]:


clusters_df['CustomerID'] = data['CustomerID']


# In[17]:


clusters_df.to_csv('./results/segments_rfm_optional.csv', index=False)

