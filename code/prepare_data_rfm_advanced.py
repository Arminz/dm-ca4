
# coding: utf-8

# In[105]:


import pandas as pd


# In[106]:


data = pd.read_csv('./data/onlineOrder_cleaned.csv')


# In[107]:


data.head(1)


# In[108]:


customers_df = pd.DataFrame()


# In[109]:


groups = data.groupby('CustomerID').max()['InvoiceDate']
customers_df['CustomerID'] = groups.index
customers_df['maxInvoiceDate'] = groups.values


# In[124]:


from datetime import datetime
NOW_TIME_STR = '2012'
now_time = datetime.strptime(NOW_TIME_STR, '%Y')


# In[125]:


import numpy as np

customers_df['recency_delta'] = np.array([now_time] * len(customers_df)) - pd.to_datetime(customers_df['maxInvoiceDate'])
value_columns = []
value_columns.append('RecencyValue')
customers_df['RecencyValue'] = customers_df['recency_delta'].dt.days


# In[126]:


# customers_df.head(1)


# In[114]:


seasons = ['2010-12-01 08:26:00', '2011-03-01 08:26:00', '2011-06-01 08:26:00',
           '2011-09-01 08:26:00', '2011-12-09 12:50:00']


# In[115]:


for i in range(len(seasons) - 1):
    time_select = (data['InvoiceDate'] > seasons[i]) & (data['InvoiceDate'] < seasons[i + 1])
    # data[time_select].groupby('CustomerID').count()['Quantity'][customers_df['CustomerID']].values
    value_columns.append('FrequencyValue_{}_{}'.format(seasons[i], seasons[i+1]))
    customers_df['FrequencyValue_{}_{}'.format(seasons[i], seasons[i+1])] = data[time_select].groupby('CustomerID').count()['Quantity'][customers_df['CustomerID']].values


# In[116]:


data['price'] = data['UnitPrice'] * data['Quantity']
for i in range(len(seasons) - 1):
    time_select = (data['InvoiceDate'] > seasons[i]) & (data['InvoiceDate'] < seasons[i + 1])
    value_columns.append('MonetaryValue_{}_{}'.format(seasons[i], seasons[i+1]))
    customers_df['MonetaryValue_{}_{}'.format(seasons[i], seasons[i + 1])] =        data[time_select].groupby('CustomerID').sum()['price'][customers_df['CustomerID']].values


# In[117]:


customers_df.fillna(0, inplace=True)


# In[118]:


# customers_df.describe()


# In[119]:


customers_df.head(4)


# In[120]:


import matplotlib.pyplot as plt
# get_ipython().run_line_magic('matplotlib', 'inline')


# In[121]:


score_columns = []
for column in value_columns:
    median = np.median(customers_df[column])
    quarth1 = np.median(customers_df[customers_df[column] <= median][column])
    quarth3 = np.median(customers_df[customers_df[column] > median][column])
    score_columns.append(column + '_' + 'score')
    customers_df[column + '_' + 'score'] = customers_df[column].apply(lambda x: int((x > median) + (x > quarth1) + (x > quarth3)))


# In[101]:


customers_df['mean'] = round(customers_df[score_columns].mean(axis=1), 2)
score_columns.append('mean')


# In[122]:


# customers_df.describe()
# customers_df.columns


# In[123]:


customers_df[['CustomerID'] + score_columns].to_csv('./data/prepared/prepared_rfm_advanced.csv', index=False) 

