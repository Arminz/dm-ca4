
# coding: utf-8

# In[1]:


import pandas as pd


# In[3]:


data = pd.read_csv('./data/onlineOrder_cleaned.csv')


# In[4]:


# data.head(1)


# In[5]:


customers_df = pd.DataFrame()


# In[6]:


groups = data.groupby('CustomerID').max()['InvoiceDate']
customers_df['CustomerID'] = groups.index
customers_df['maxInvoiceDate'] = groups.values


# In[7]:


from datetime import datetime
NOW_TIME_STR = '2012'
now_time = datetime.strptime(NOW_TIME_STR, '%Y')


# In[8]:


import numpy as np

# customers_df['recency'] = np.array([pd.datetime.now()] * len(customers_df)) - pd.to_datetime(customers_df['maxInvoiceDate'])
customers_df['recency_delta'] = np.array([now_time] * len(customers_df)) - pd.to_datetime(customers_df['maxInvoiceDate'])
customers_df['RecencyValue'] = customers_df['recency_delta'].dt.days


# In[9]:


customers_df.head(1)


# In[10]:


MIN_TIME_STR = '2010-12-01 08:26:00'
MAX_TIME_STR = '2011-12-09 12:50:00'


# In[11]:


time_select = (data['InvoiceDate'] > MIN_TIME_STR) & (data['InvoiceDate'] < MAX_TIME_STR)


# In[20]:


customers_df['FrequencyValue'] = data[time_select].groupby('CustomerID').count()['Quantity'].values


# In[13]:


data['price'] = data['UnitPrice'] * data['Quantity']
customers_df['MonetaryValue'] = data[time_select].groupby('CustomerID').sum()['price'][customers_df['CustomerID']].values


# In[14]:


# (data[time_select].groupby('CustomerID').sum()['price'] == data[time_select].groupby('CustomerID').sum()['price'][customers_df['CustomerID']]).all()


# In[15]:


customers_df.head(4)


# In[16]:


customers_df.describe()


# In[17]:


# import matplotlib.pyplot as plt
# get_ipython().run_line_magic('matplotlib', 'inline')


# In[21]:


# plt.hist(customers_df['FrequencyValue'], bins=30)


# In[30]:


customers_df['FrequencyScore'] = customers_df['FrequencyValue'].apply(lambda x: (x > 17) + (x > 41) + (x > 100))


# In[31]:


# plt.hist(customers_df['FrequencyScore'])


# In[33]:


# plt.hist(customers_df['RecencyValue'])


# In[37]:




# In[29]:


customers_df['MonetaryScore'] = customers_df['MonetaryValue'].apply(lambda x: (x > 307) + (x > 673) + (x > 1661))


# In[25]:


# selected = customers_df['monetary_value'] < 10000 
# plt.hist(customers_df['MonetaryValue'])


# In[138]:


# customers_df2 = pd.DataFrame(minMaxScaler
#   .fit_transform(customers_df[['recency_normal', 'frequency_normal', 
#   'monetary_value_normal']]) .astype(int), columns=[['recency_normal', 'frequency_normal', 'monetary_value_normal']]) 


# In[40]:


# from sklearn.preprocessing import MinMaxScaler
# minMaxScaler = MinMaxScaler((0, 3))


# In[27]:


customers_df['RecencyScore'] = customers_df['RecencyValue'].apply(lambda x: (x > 39) + (x > 72) + (x > 163))
# customers_df['recency_normal'] = minMaxScaler.fit_transform(np.log(customers_df['recency']).reshape(-1, 1))
# customers_df2 = pd.DataFrame(minMaxScaler
#                              .fit_transform(customers_df[['recency', 'frequency', 'monetary_value']])
#                              .astype(int), columns=[['recency_normal', 'frequency_normal', 'monetary_value_normal']])


# In[34]:


# plt.hist(customers_df['RecencyScore'])


# In[35]:


# customers_df.describe()


# In[36]:


# customers_df2.describe()


# In[37]:


# customers_df2['recency_normal']


# In[38]:


customers_df[['CustomerID', 'RecencyScore', 'FrequencyScore', 'MonetaryScore']]    .to_csv('./data/prepared/prepared_rfm_1.csv', index=False)

