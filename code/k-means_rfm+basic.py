
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
# get_ipython().run_line_magic('matplotlib', 'inline')


# In[18]:


data_1 = pd.read_csv('./data/prepared/prepared_basic.csv')
data_2 = pd.read_csv('./data/prepared/prepared_rfm_advanced.csv')


# In[19]:


# data_2.head(1)


# In[20]:


# print(data_1.head(1))


# In[21]:


data = data_1.join(data_2.set_index('CustomerID'), on='CustomerID')


# In[22]:


data.to_csv('./data/prepared/prepared_rfm+basics.csv', index=False)


# In[ ]:


from sklearn.cluster import KMeans  
from sklearn.metrics import silhouette_score


# In[ ]:


K_values = range(2, 10, 1)
progress = 0
scores = []
for K in K_values:
    kmeans = KMeans(n_clusters=K)
    kmeans.fit(data)
    # clusters = kmeans.transform(data)
    score = silhouette_score(data, kmeans.labels_)
    scores.append(score)
    
    progress += 1
    print ('\r' + str(int(100 * progress / len(K_values))), end='%')

    
    


# In[ ]:


# plt.plot(K_values, scores)


# In[17]:


# scores, list(K_values)


# In[31]:


best_value = K_values[[i for i, j in enumerate(scores) if j == max(scores)][0]]
print('best_value: ' + str(best_value))
kmeans = KMeans(n_clusters=best_value)
kmeans.fit(data)


# In[12]:


clusters_df = pd.DataFrame()


# In[13]:


clusters_df['SegmentNo'] = kmeans.labels_


# In[15]:


clusters_df['CustomerID'] = data['CustomerID']


# In[16]:


clusters_df.to_csv('./results/segments_rfm+basic.csv', index=False)

